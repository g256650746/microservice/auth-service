package uz.uzum.authservice.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import uz.uzum.authservice.entity.User;
import uz.uzum.authservice.payload.ApiResult;
import uz.uzum.authservice.payload.LoginDTO;
import uz.uzum.authservice.payload.RegisterDTO;
import uz.uzum.authservice.service.AuthService;
import uz.uzum.authservice.utils.AppConstants;

@RestController
@RequestMapping(AuthController.BASE_PATH)
@RequiredArgsConstructor
public class AuthController {

    public static final String BASE_PATH = AppConstants.BASE_PATH+"/auth";
    public static final String REGISTER_PATH = "/register";
    private final AuthService authService;

    @PostMapping(REGISTER_PATH)
    public HttpEntity<ApiResult<User>> signUp(@RequestBody RegisterDTO registerDTO) {
        ApiResult<User> result = authService.register(registerDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED.value())
                .body(result);
    }


    @PostMapping("/login")
    public HttpEntity<?> signIn(@Valid @RequestBody LoginDTO loginDTO) {
        String token = authService.login(loginDTO);
        return ResponseEntity.status(HttpStatus.OK.value()).body(token);
    }

}
