package uz.uzum.authservice.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.uzum.authservice.entity.User;
import uz.uzum.authservice.payload.UserDTO;
import uz.uzum.authservice.security.UserPrincipal;

@RestController
@RequestMapping("/api/auth/user")
public class UserController {


    @GetMapping("/validate-token")
    public UserDTO userMe() {
        User user = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).user();
        return new UserDTO(user.getId(), user.getName(), user.getRole().getPermissions());
    }
}
