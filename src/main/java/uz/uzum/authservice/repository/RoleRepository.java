package uz.uzum.authservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.uzum.authservice.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByUserRoleTrue();
}