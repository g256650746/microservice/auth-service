package uz.uzum.authservice.mapper;

import org.mapstruct.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import uz.uzum.authservice.entity.User;
import uz.uzum.authservice.payload.RegisterDTO;

@Mapper(componentModel = "spring")
public interface UserMapper {

    //        @Mapping(target = "password", expression = "java(passwordEncoder.encode(registerDTO.getPassword()))")
    @Mapping(target = "password", ignore = true)
    @Mapping(target = "name", source = "ism", qualifiedByName = "upper")
    User mapFromRegisterDTOToUser(RegisterDTO registerDTO, @Context PasswordEncoder passwordEncoder);


    @Named(value = "upper")
    default String ismKatta(String ism) {
        return ism.toUpperCase();
    }

    @AfterMapping
    default void map(@MappingTarget User user, RegisterDTO registerDTO, @Context PasswordEncoder passwordEncoder) {
        user.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
    }

}
