package uz.uzum.authservice.service;

import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.uzum.authservice.entity.User;
import uz.uzum.authservice.mapper.UserMapper;
import uz.uzum.authservice.payload.ApiResult;
import uz.uzum.authservice.payload.LoginDTO;
import uz.uzum.authservice.payload.RegisterDTO;
import uz.uzum.authservice.repository.RoleRepository;
import uz.uzum.authservice.repository.UserRepository;
import uz.uzum.authservice.security.JWTProvider;
import uz.uzum.authservice.security.UserPrincipal;

import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {

    public static final String key = "7#70OZn0#Dsao8989DA1%T&hBnrzgqK28o";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    private final UserMapper userMapper;

    private final PasswordEncoder passwordEncoder;

    private final JWTProvider jwtProvider;
    private final AuthenticationManager authenticationManager;

    public AuthService(UserRepository userRepository,
                       RoleRepository roleRepository, UserMapper userMapper,
                       @Lazy PasswordEncoder passwordEncoder,
                       JWTProvider jwtProvider,
                       @Lazy AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.jwtProvider = jwtProvider;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new UserPrincipal(user);
    }

    public ApiResult<User> register(RegisterDTO registerDTO) {
        if (userRepository.existsByEmail(registerDTO.getEmail()))
            throw new RuntimeException("User already exists");

        User user = userMapper.mapFromRegisterDTOToUser(registerDTO, passwordEncoder);
        user.setRole(roleRepository.findByUserRoleTrue().orElseThrow());

        return ApiResult.successResponse(userRepository.save(user));
    }

    public String login(LoginDTO loginDTO) {

        UserPrincipal principal = checkCredential(loginDTO.getEmail(), loginDTO.getPassword());

        return jwtProvider.createAccessToken(principal.user().getId().toString());
    }

    public Optional<User> findUserById(String userId) {
        return userRepository.findById(UUID.fromString(userId));
    }


    public UserPrincipal checkCredential(String username, String password) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = authenticationManager.authenticate(authenticationToken);

        return (UserPrincipal) authentication.getPrincipal();
    }
}
