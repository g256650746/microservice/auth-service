package uz.uzum.authservice.enums;

public enum PermissionEnum {

    ADD_PRODUCT,
    EDIT_PRODUCT,
    DELETE_PRODUCT,
    ALL_PRODUCT,
    LIST_ORDER,
    CANCEL_ORDER,
    CANCEL_SHIPPING
}
