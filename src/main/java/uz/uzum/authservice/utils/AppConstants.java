package uz.uzum.authservice.utils;

public interface AppConstants {

    String BASE_PATH = "/api/auth";
    String BEARER_TYPE = "Bearer";
    String BASIC_TYPE = "Basic";
}
