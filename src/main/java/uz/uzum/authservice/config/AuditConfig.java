package uz.uzum.authservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import uz.uzum.authservice.entity.User;

@EnableJpaAuditing
@Configuration
public class AuditConfig {

    @Bean
    public AuditorAware<User> auditorAware() {
        return new AuditingAware();
    }

}
